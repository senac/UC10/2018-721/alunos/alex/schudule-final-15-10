package br.com.senac.view;

import br.com.senac.view.JFrameLogin;


public class JFrameApp {
     public static void main(String args[]) {
     
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameLogin().setVisible(true);
            }
        });
        
        
    }
}
