package br.com.senac.persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao {

    private static final String URL = "jdbc:mysql://localhost:3306/BancoAgenda";
    private static final String USER = "root";
    private static final String PASSWORD = "123456";
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    public static Connection getConnection() {

        Connection connection = null;

        try {
            // careegando o driver do banco 
            Class.forName(DRIVER);
            // Abrir conexao com o banco 
            connection = (Connection) DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Conectou !!!!!");

        } catch (ClassNotFoundException ex) {
            System.out.println("Erro ao carregar o Driver do banco.");
            ex.printStackTrace();

        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Erro ao Conectar no banco");
        }

        return connection;
    }

    public static void main(String[] args) {
        Conexao.getConnection();
    }

    //url
    //usuario
    //senha
    //driver
}
